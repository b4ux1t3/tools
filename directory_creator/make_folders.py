import sys
import os

folder_names = []
folder_path = None
try:
    with open(sys.argv[1], "r") as input_file:
        folder_names = input_file.readlines()
    
    folder_path = sys.argv[2]
except:
    print(f"Usage: python3 {__file__} [list of directory names] [path to save]")

folder_names = [name.strip() for name in folder_names]

for name in folder_names:
    if not os.path.exists(folder_path + name):
        try:
            new_folder = os.path.join(folder_path, name)
            print(f"Attempting to create: {new_folder}")
            os.mkdir(new_folder)
        except OSError as err:
            print(f"Unable to create directory {folder_path + name}: {err}\nThis is likely due to an invalid character in the directory name.")
            continue
    else:
        if os.path.isdir(folder_path + name):
            print(f"Directory {folder_path + name} already exists.")
        else:
            print(f"File {folder_path + name} already exists.")
