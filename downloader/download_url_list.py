import requests
import os
import sys
from clint.textui import progress


to_download = []
filename = None

#TODO: Substitute this for flags.
#TODO: Add flag for output directory.
try:
    filename = sys.argv[1]
except IndexError:
    print('Usage: python download_url_list.py filename [data_directory]')
    sys.exit(1)
# End CLI interaction

with open(filename, 'r') as infile:
    for line in infile:
        to_download.append(line.strip())

counter = 0
len_list = len(to_download)

data_directory = ""
if len(sys.argv) > 2:
    data_directory = sys.argv[2]
else:
    data_directory = "data"

if not (os.path.exists(data_directory)):
    os.mkdir(data_directory)
if not os.path.isdir(data_directory):
    print('Data directory argument must be a directory!')
    sys.exit(2)

#TODO: Break this into functions.
for url in to_download:
    
    name = url.split('/')[-1]
    try:
        if not os.path.exists(f'{data_directory}/{name}'):
            counter += 1
            print(f'Downloading {name}\nFile {counter} out of {len_list}')
            r = requests.get(url, stream=True) # Stream brings it down in chunks and lets us fail gracefully. 
            with open(f'{data_directory}/{name}', 'wb') as outfile:
                total_length = int(r.headers.get('content-length'))
                for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1):  # Stream in requests.get also lets us get a progress bar. Woo hoo!
                    if chunk:
                        outfile.write(chunk)
                        outfile.flush()        
        else:
            print(f'{name} exists.')
    
    except requests.ConnectionError as err:
        print(f'Connection error with {name}')
        os.remove(f'{data_directory}/{name}')
        with open('not_downloaded.txt', 'w') as errors:
            errors.write(f'{url}\n')
        continue
    except OSError as err:
        print(f'Operating System error with {name}. Likely network was lost.')
        os.remove(f'{data_directory}/{name}')
        with open('not_downloaded.txt', 'w') as errors:
            # Write the files in the list that were not downloaded to a file
            for line in to_download[counter - 1:]: # Counter is equal to the index of where we are + 1, since we increment it in every loop efore we start
                errors.write(f'{line}\n')
        sys.exit(3)
    except KeyboardInterrupt:
        print(f'User interrupted program download of {name}.')
        os.remove(f'{data_directory}/{name}')
        with open('not_downloaded.txt', 'w') as errors:
            for line in to_download[counter - 1:]:
                errors.write(f'{line}\n')
        sys.exit(2)
